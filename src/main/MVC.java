package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;
import controller.Controller;
import vo.InterseccionMallaVial;

public class MVC 
{

	public static void main(String[] args) 
	{
		Controller controler = new Controller();
		controler.run();
	}
	
}
