
package controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;

import java.util.Scanner;

import org.json.simple.parser.ParseException;

import data_structures.Graph;
import data_structures.IQueue;

import data_structures.MaxHeapCP;
import data_structures.Queue;
import jdk.nashorn.internal.runtime.arrays.NumericElements;
import logic.MovingViolationsManager;
import view.MovingViolationsManagerView;
import vo.Arcos_Calles;
import vo.InterseccionMallaVial;

public class Controller {

	// Componente vista (consola)

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */

	private IQueue<InterseccionMallaVial> colaList = new Queue<InterseccionMallaVial>();

	Queue<InterseccionMallaVial> vertices = new Queue<InterseccionMallaVial>();

	Queue<Arcos_Calles> arcos = new Queue<Arcos_Calles>();
	
	int numeroDeArcosCola = 0;

	Graph grafo;

	private MovingViolationsManagerView view;

	// Componente modelo (logica de la aplicacion)
	private MovingViolationsManager manager;

	public final static String VIOLATIONDESCR = "violationdescription";
	public final static String STREETID = "streetid";
	public final static String TICKETISSUEDATE = "ticketissuedate";
	public final static String XCOORD = "xcoord";
	public final static String YCOORD = "ycoord";
	public final static String FECHA = "fecha";

	public Controller() {
		view = new MovingViolationsManagerView();
		manager = new MovingViolationsManager();
		grafo = new Graph();
		
	}

	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		Controller controller = new Controller();

		long startTime;
		long endTime;
		long duration;

		while (!fin) {

			view.printMenu();

			int option = sc.nextInt();

			switch (option) {
			case 0:
				manager.cargar("./data/Central-WashingtonDC-OpenStreetMap.xml");
				vertices = manager.darVertices();
				arcos = manager.darArcos();
				grafo = new Graph();
				System.out.println(" - - - - - - - - - - - - -");
				System.out.println("se cargaron en total : " + arcos.size() + " Arcos con Highway (desde el xml)");
				numeroDeArcosCola = arcos.size();
				cargarGrafo();
				System.out.println("se cargaron en total : " + grafo.V() + " Vertices");
				System.out.println("se cargaron en total : " + grafo.E() + " Arcos en el grafo (ya concectados todos los vertices con todos los arcos)");

				break;

			case 1:
				manager.EscribirJSON(grafo.darVertices(), grafo.darArcos(), darNumeroArcosOriginal());
				System.out.println("Se ha generado el documento con formato JSON correctamente");
				break;

			case 2:
				view.printMessage("Dar ruta del archivo JSON: " + "Ej. " + "./ArchivoEnJSON.json" );
				String ruta = sc.next();
				try {
					manager.leerJson(ruta);
		
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

	}

	public void cargarGrafo() {
		

		while (!vertices.isEmpty()) {

			InterseccionMallaVial actual = vertices.dequeue();

			grafo.addVertex(actual.objectId(), actual);

		}

		while (!arcos.isEmpty()) {

			Arcos_Calles arcoActual = arcos.dequeue();

			for (int i = 0; i < arcoActual.darVertices().size(); i++) {

				String verticeActual = arcoActual.darVertices().get(i);

				for (int j = i + 1; j < arcoActual.darVertices().size(); j++) {

					String verticeSegundo = arcoActual.darVertices().get(j);

					grafo.addEdge2(verticeActual, verticeSegundo, arcoActual);
				}

			}

		}

	}
	
	public int darNumeroArcosOriginal() {
		return numeroDeArcosCola;
	}

}
