package view;

import java.util.ArrayList;

import data_structures.IQueue;
import data_structures.Queue;

public class MovingViolationsManagerView 
{
	/**
	 * Constante con el número maximo de datos maximo que se deben imprimir en consola
	 */
	public static final int N = 20;

	public void printMenu() 
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("-------------------Taler 8--------------------");
		System.out.println("0. Cargar malla vial de Washington D.C en grafo no dirigido -- dar # vertices y # arcos del grafo inicial");
		System.out.println("1. Escribir archivo JSON");
		System.out.println("2. Cargar archivo JSON.");
		System.out.println("3. Salir");
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}

	public void printMessage(String mensaje) 
	{
		System.out.println(mensaje);
	}


}
