package vo;

import java.util.ArrayList;

import data_structures.IQueue;
import data_structures.Queue;

public class Arcos_Calles{
	/**
	 * representa el ID 
	 */
	private String objectID;	
	private ArrayList<String>vertices = new ArrayList<>();

	

	public Arcos_Calles (String pObjectID,ArrayList<String> verticesActual) 
	{
		objectID =pObjectID;
		vertices = verticesActual;
	}

	/**
	 * @return id - Identificador único de la infracción
	 */
	public String objectId() 
	{
		return objectID;
	}
	public ArrayList<String> darVertices() 
	{
		return vertices;
	}
	
	public void setId(String pId)
	{
		objectID = pId;
	}
	
	public void setVertices(ArrayList<String> nuevosVertices)
	{
		vertices = nuevosVertices;
	}
	
	
	public String toString() {
		return ("Identificador : " + objectID + "; vertices : " + vertices.toString().replace(",", "-"));
	}
	
}
