package vo;
/**
 * Representation of a Trip object
 */
public class InterseccionMallaVial implements Comparable<InterseccionMallaVial>
{
	/**
	 * representa el ID 
	 */
	private String objectID;	
	private double latitud;
	private double longitud;
	

	public InterseccionMallaVial(String pObjectID,String pLatitud, String pLongitud) 
	{
		objectID = pObjectID;
		latitud = Double.parseDouble(pLatitud);
		longitud = Double.parseDouble(pLongitud);
	}

	/**
	 * @return id - Identificador único de la infracción
	 */
	public String objectId() 
	{
		return objectID;
	}
	public double getLatitud() 
	{
		return latitud;
	}
	
	public double getLongitud()
	{
		return longitud;
	}
	public void setId(String id)
	{
		objectID = id;
	}
	
	public void setLongitud(double newLongitud)
	{
		longitud = newLongitud;
	}
	
	public void setLatitud(double newLatitud)
	{
		latitud = newLatitud;
	}
	public int compareTo(InterseccionMallaVial dato) {
		
		int RTA  =0;
		
		if (this.objectID.compareTo(dato.objectID) < 0 ) {
			RTA = -1; 
		}
		else if (this.objectID.compareTo(dato.objectID) > 0) {
			RTA = 1;
		}
		return RTA;
		
	}
	
	public String toString() {
		return ("Identificador : " + objectID + "; Latitud : " + latitud + "; Longitud : " + longitud);
	}
	
}
