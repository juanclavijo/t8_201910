package logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import controller.Controller;
import data_structures.IMaxHeapCP;
import data_structures.IQueue;
import data_structures.MaxHeapCP;
import data_structures.Queue;
import data_structures.RedBlackBalancedSearchTrees;
import vo.Arcos_Calles;
import vo.InterseccionMallaVial;

public class MovingViolationsManager {

	Queue<InterseccionMallaVial> vertices = new Queue<InterseccionMallaVial>();

	Queue<Arcos_Calles> arcos = new Queue<Arcos_Calles>();

	int contador = 0;

	public MovingViolationsManager() {

	}

	public void cargar(String pRuta) {

		File archivo = new File(pRuta);

		int cont = 0;

		try {
			@SuppressWarnings("resource")
			Scanner sca = new Scanner(archivo);
			sca.nextLine();

			while (sca.hasNext()) {

				String lineaActual = sca.next();

				if (lineaActual.equals("<node")) {

					String identificadorActual = sca.next();
					String latitudActual = sca.next();
					String longitudActual = sca.next();

					identificadorActual = identificadorActual.replaceAll("\"", "");
					identificadorActual = identificadorActual.replaceAll("\\s", "");
					identificadorActual = identificadorActual.split("=")[1];

					latitudActual = latitudActual.replaceAll("\"", "");
					latitudActual = latitudActual.split("=")[1];

					longitudActual = longitudActual.replaceAll("\"", "");
					longitudActual = longitudActual.split("=")[1];

					InterseccionMallaVial nuevo = new InterseccionMallaVial(identificadorActual, latitudActual,
							longitudActual);
					vertices.enqueue(nuevo);

					contador++;

				}

				else if (lineaActual.equals("<way")) {

					String identificadorActual = sca.next();

					ArrayList<String> arregloDelActual = new ArrayList<String>();

					identificadorActual = identificadorActual.replaceAll("\"", "");
					identificadorActual = identificadorActual.replaceAll("\\s", "");
					identificadorActual = identificadorActual.split("=")[1];

					String referencia = sca.next();

					boolean tieneHighWay = false;

					while (!referencia.equals("</way>") && !tieneHighWay) {

						if (referencia.equals("<nd")) {

							String referenciaActual = sca.next();
							referenciaActual = referenciaActual.replaceAll("\"", "");
							referenciaActual = referenciaActual.replaceAll("/>", "");
							referenciaActual = referenciaActual.replaceAll("\\s", "");
							referenciaActual = referenciaActual.split("=")[1];

							arregloDelActual.add(referenciaActual);

						}

						else if (referencia.equals("<tag")) {

							String tagActual = sca.next();
							tagActual = tagActual.replaceAll("\"", "");
							tagActual = tagActual.replaceAll(":", "");
							tagActual = tagActual.replaceAll("\\s", "");
							tagActual = tagActual.split("=")[1];

							if (tagActual.equals("highway")) {

								tieneHighWay = true;

								Arcos_Calles nuevoArco = new Arcos_Calles(identificadorActual, arregloDelActual);

								arcos.enqueue(nuevoArco);

							}

						}

						referencia = sca.next();

					}

				}

			}

		}

		catch (Exception e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	public void EscribirJSON(RedBlackBalancedSearchTrees<String, InterseccionMallaVial> verticesActuales,
			RedBlackBalancedSearchTrees<String, Arcos_Calles> arcosActuales, int numeroArcosOriginal) {

		JSONObject obj = new JSONObject();

		// Se añaden los vertices

		JSONArray listaVertices = new JSONArray();
		
		Iterator<String> it  = verticesActuales.keys().iterator();
		
		
		for (int i = 0; i < verticesActuales.size(); i++) {
			
		
			String key = it.next();
			

			InterseccionMallaVial actual = verticesActuales.get(key);

			JSONObject objetoActual = new JSONObject();

			objetoActual.put("Vertice", actual.toString());

			listaVertices.add(objetoActual);

		}


		JSONArray listaArcos = new JSONArray();
		
		Iterator<String> it2  = arcosActuales.keys().iterator();
		
		for (int i = 0; i <numeroArcosOriginal ; i++) {
			
			String key = it2.next();
			

			Arcos_Calles actual = arcosActuales.get(key);

			JSONObject objetoActual = new JSONObject();

			objetoActual.put("Arco", actual.toString());

			listaArcos.add(objetoActual);
			

		}

		obj.put("Vertices ", listaVertices);
		obj.put("Arcos", listaArcos);

		try {

			FileWriter file = new FileWriter("./ArchivoEnJSON.json");
			file.write(obj.toJSONString());
			file.flush();
			file.close();

		} catch (IOException e) {
			// manejar error
		}

	}

	public void leerJson(String pRuta) throws IOException, ParseException {

		FileInputStream fileInputStream = null;
		String data="";
		StringBuffer stringBuffer = new StringBuffer("");
		try{
		    fileInputStream=new FileInputStream(pRuta);
		    int i;
		    while((i=fileInputStream.read())!=-1)
		    {
		        stringBuffer.append((char)i);
		    }
		    data = stringBuffer.toString();
		}
		catch(Exception e){
		        e.printStackTrace();
		}
		finally{
		    if(fileInputStream!=null){  
		        fileInputStream.close();
		    }
		}
		
		
		//PRIMERA PARTE : VERTICES
		
		String[] grande = data.split("],");
		
		String vertices2 = grande[0];
		
		String arcos2  = grande[1];
		
		
		vertices2 = vertices2.replace("[", "@");
		
		vertices2 = vertices2.split("@")[1];
		
		
		String[] datosVertice = vertices2.split(",");
		
		
		for (int i = 0; i < datosVertice.length; i++) {
		
			String lineaActual = datosVertice[i];
			
			
			lineaActual = lineaActual.replace("{\"", "");
			lineaActual = lineaActual.replace("\"}", "");
			lineaActual = lineaActual.replace("\":\"", "");
			String[] datosMasPequeños = lineaActual.split(";");
			
					
				String identificador = datosMasPequeños[0].split(":")[1];
				
				String latitud = datosMasPequeños[1].split(":")[1];
				
				String longitud = datosMasPequeños[2].split(":")[1];
				
			
				InterseccionMallaVial nuevo = new InterseccionMallaVial(identificador, latitud,longitud);
				vertices.enqueue(nuevo);
			
		}
		
		
		//SEGUNA PARTE : ARCOS
		
		
		arcos2 = arcos2.replace("]}", "");
		arcos2 = arcos2.replace("Arcos\":[", "");
		
		String [] datosArcos = arcos2.split(",");
		
		
		for (int i = 0; i < datosArcos.length; i++) {
			
			String lineaActual = datosArcos[i];
			
			lineaActual = lineaActual.replace("{\"", "");
			lineaActual = lineaActual.replace("\"}", "");
			lineaActual = lineaActual.replace("\":\"", "");
			
			String[] datosMasPequeños = lineaActual.split(";");
			
			String identificador = datosMasPequeños[0].split(":")[1];
			
			String otrosDatos = datosMasPequeños[1].replace("vertices : [", "");
			otrosDatos = otrosDatos.replace("]", "");
			
			String[] paraArreglo = otrosDatos.split("-");
			
			
			ArrayList<String> delActual = new ArrayList<>();
			
			for (int j = 0; j < paraArreglo.length; j++) {
				delActual.add(paraArreglo[j]);
			}
			
			Arcos_Calles nuevoArco = new Arcos_Calles(identificador, delActual);

			arcos.enqueue(nuevoArco);
			
			
		}
		
		
		System.out.println("Se leyeron en total : " + vertices.size() + "vertices");
		System.out.println("Se leyeron en total : " + arcos.size() + "arcos posibles de armar (que tienen Highway)");
		
		

	}

	public Queue<InterseccionMallaVial> darVertices() {
		return vertices;
	}

	public Queue<Arcos_Calles> darArcos() {
		return arcos;
	}

}